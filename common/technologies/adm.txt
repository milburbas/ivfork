monarch_power = ADM

# applied when a country is ahead of time in this technology
ahead_of_time = {
	production_efficiency = 0.2
	yearly_corruption = -0.05
}

# Pre-era techs
technology = {
	# Tech 0
	year = 1350
	
	tribal_despotism_legacy = yes 	
	tribal_federation_legacy = yes 
	
	native_earthwork = yes
	native_palisade = yes
	native_fortified_house = yes
	native_three_sisters_field = yes
	native_irrigation = yes
	native_storehouse = yes
	native_longhouse = yes
	native_sweat_lodge = yes
	native_great_trail = yes
	native_ceremonial_fire_pit = yes
}

technology = {
	# Tech 1
	year = 1390
	expects_institution = {
		feudalism = 0.25
	}
	
	tribal_democracy_legacy = yes
	tribal_kingdom_legacy = yes
}

technology = {
	# Tech 2
	year = 1420
	expects_institution = {
		feudalism = 0.5
	}
	#Monarchies
	despotic_monarchy = yes	
	feudal_monarchy = yes
	oligarchic_republic = yes
}

# The 30 big techs from here on

technology = {
	# Tech 3
	year = 1440
	allowed_idea_groups = 1
	expects_institution = {
		feudalism = 0.5
	}
	production_efficiency	=	0.02
	may_support_rebels = yes

	 infrastructure_lvl_1 = yes
	 
}

technology = {
	# Tech 4
	year = 1453
	expects_institution = {
		feudalism = 0.5
	}
	# Temple
	temple = yes
	#taxation_lvl_1 = yes
	terrain_land = yes
	terrain_woodland = yes
	terrain_hills = yes
	allowed_idea_groups = 2
	
}

technology = {
	# Tech 5
	year = 1466
	expects_institution = {
		feudalism = 0.5
		renaissance = 0.15
	}
	 
	terrain_desert = yes
	terrain_art = yes
	terrain_coastal = yes
	terrain_swamp = yes
	terrain_jungle = yes	
	allowed_idea_groups = 3
	production_efficiency	=	0.02
}

technology = {
	# Tech 6
	year = 1479
	#government_lvl_1 = yes
	courthouse = yes
	#production_lvl_1 = yes
	expects_institution = {
		feudalism = 0.5
		renaissance = 0.3
	}
	
	#Workshop
	farm_estate = yes
	workshop = yes
	ramparts = yes
}

technology = {
	# Tech 7
	year = 1492
	university = yes # University
	expects_institution = {
		feudalism = 0.5
		renaissance = 0.5
	}
	#Noble Republic
	allowed_idea_groups = 4
}

technology = {
	# Tech 8
	infrastructure_lvl_2 = yes
	year = 1505
	allowed_idea_groups = 5
	expects_institution = {
		feudalism = 0.5
		renaissance = 0.5
	}
	governing_capacity = 100
	#courthouse = yes

}

technology = {
	# Tech 9
	year = 1518
	production_lvl_2 = yes
	expects_institution = {
		feudalism = 0.5
		renaissance = 0.5
		new_world_i = 0.15
	}
	#The Scythe
		production_efficiency	=	0.02
	

}

technology = {
	# Tech 10
	year = 1531
	#government_lvl_2 = yes
	town_hall = yes
	allowed_idea_groups = 6
	expects_institution = {
		feudalism = 0.5
		renaissance = 0.5
		new_world_i = 0.3
	}
	theocratic_government = yes
}

technology = {
	# Tech 11
	year = 1544
	taxation_lvl_2 = yes
	expects_institution = {
		feudalism = 0.5
		renaissance = 0.5
		new_world_i = 0.5
	}
	#Textile Manufactory
	textile = yes  

}

technology = {
	# Tech 12
	year = 1557
	allowed_idea_groups = 7
	governing_capacity = 100
	expects_institution = {
		feudalism = 0.5
		renaissance = 0.5
		new_world_i = 0.5
	}
	administrative_monarchy	= yes 
	administrative_republic	= yes
	state_house = yes
}

technology = {
	# Tech 13
	year = 1570
	expects_institution = {
		feudalism = 0.5
		renaissance = 0.5
		new_world_i = 0.5
		printing_press = 0.15
	}
	
	#Improved Drainage
		production_efficiency	=	0.02
}

technology = {
	# Tech 14
	year = 1583
	#taxation_lvl_3 = yes
	cathedral = yes
	expects_institution = {
		feudalism = 0.5
		renaissance = 0.5
		new_world_i = 0.5
		printing_press = 0.3
	}
	plantations = yes
	allowed_idea_groups	= 8
}

technology = {
	# Tech 15
	year = 1596
	infrastructure_lvl_3 = yes
	expects_institution = {
		feudalism = 0.5
		renaissance = 0.5
		new_world_i = 0.5
		printing_press = 0.5
	}
	may_force_march = yes
	soldier_households = yes
	
}

technology = {
	# Tech 16
	year = 1609
	government_lvl_3 = yes
	expects_institution = {
		feudalism = 0.5
		renaissance = 0.5
		new_world_i = 0.5
		printing_press = 0.5
	}
	mills = yes
	allowed_idea_groups = 9
	production_efficiency	=	0.02


}

technology = {
	# Tech 17
	year = 1622
	production_lvl_3 = yes
	#university = yes
	expects_institution = {
		feudalism = 0.5
		renaissance = 0.5
		new_world_i = 0.5
		printing_press = 0.5
		global_trade = 0.15
	}
	governing_capacity = 250
	
	administrative_efficiency = 0.1
	development_efficiency = 0.1
}

technology = {
	# Tech 18
	year = 1635
	expects_institution = {
		feudalism = 0.5
		renaissance = 0.5
		new_world_i = 0.5
		printing_press = 0.5
		global_trade = 0.3
	}
	
	allowed_idea_groups	= 10
}

technology = {
	# Tech 19
	year = 1648	
	expects_institution = {
		feudalism = 0.5
		renaissance = 0.5
		new_world_i = 0.5
		printing_press = 0.5
		global_trade = 0.5
	}
	
	taxation_lvl_4 = yes
	#Cathedral
	#cathedral = yes
	
}

technology = {
	# Tech 20
	year = 1661
	government_lvl_4 = yes
	allowed_idea_groups = 11
	expects_institution = {
		feudalism = 0.5
		renaissance = 0.5
		new_world_i = 0.5
		printing_press = 0.5
		global_trade = 0.5
	}
	republican_dictatorship = yes
	governing_capacity = 250
}

technology = {
	# Tech 21
	year = 1674
	furnace = yes
	expects_institution = {
		feudalism = 0.5
		renaissance = 0.5
		new_world_i = 0.5
		printing_press = 0.5
		global_trade = 0.5
		manufactories = 0.15
	}
	#Land Clearance
	production_efficiency	=	0.02
		
}

technology = {
	# Tech 22
	year = 1687
	#production_lvl_4 = yes
	counting_house = yes  
	allowed_idea_groups = 12
	expects_institution = {
		feudalism = 0.5
		renaissance = 0.5
		new_world_i = 0.5
		printing_press = 0.5
		global_trade = 0.5
		manufactories = 0.3
	}
	constitutional_monarchy = yes
	constitutional_republic = yes  

	#town_hall = yes
}

technology = {
	# Tech 23
	year = 1700
	infrastructure_lvl_4 = yes
	expects_institution = {
		feudalism = 0.5
		renaissance = 0.5
		new_world_i = 0.5
		printing_press = 0.5
		global_trade = 0.5
		manufactories = 0.5
	}
	administrative_efficiency = 0.1
	development_efficiency = 0.1
}

technology = {
	# Tech 24
	year = 1715
	allowed_idea_groups = 13
	#counting_house = yes  
	expects_institution = {
		feudalism = 0.5
		renaissance = 0.5
		new_world_i = 0.5
		printing_press = 0.5
		global_trade = 0.5
		manufactories = 0.5
		enlightenment = 0.15
	}
	governing_capacity = 250
}

technology = {
	# Tech 25
	year = 1730
	expects_institution = {
		feudalism = 0.5
		renaissance = 0.5
		new_world_i = 0.5
		printing_press = 0.5
		global_trade = 0.5
		manufactories = 0.5
		enlightenment = 0.3
	}
	
	#Improved Farm Animals
	production_efficiency	=	0.02
}

technology = {
	# Tech 26
	year = 1745
	expects_institution = {
		feudalism = 0.5
		renaissance = 0.5
		new_world_i = 0.5
		printing_press = 0.5
		global_trade = 0.5
		manufactories = 0.5
		enlightenment = 0.5
	}
	
	allowed_idea_groups 	= 	14
}

technology = {
	# Tech 27
	year = 1760
	expects_institution = {
		feudalism = 0.5
		renaissance = 0.5
		new_world_i = 0.5
		printing_press = 0.5
		global_trade = 0.5
		manufactories = 0.5
		enlightenment = 0.5
	}

	administrative_efficiency = 0.1
	development_efficiency = 0.1	
	governing_capacity = 250
}

technology = {
	# Tech 28
	year = 1775
	allowed_idea_groups = 15
	expects_institution = {
		feudalism = 0.5
		renaissance = 0.5
		new_world_i = 0.5
		printing_press = 0.5
		global_trade = 0.5
		manufactories = 0.5
		enlightenment = 0.5
		industrialization = 0.25
	}
	#Rotherham Plough
	production_efficiency	=	0.02
}

technology = {
	# Tech 29
	year = 1790
	expects_institution = {
		feudalism = 0.5
		renaissance = 0.5
		new_world_i = 0.5
		printing_press = 0.5
		global_trade = 0.5
		manufactories = 0.5
		enlightenment = 1
		industrialization = 0.5
	}
	
	allowed_idea_groups = 16
	enlightened_despotism = yes
	bureaucratic_despotism = yes
}

technology = {
	# Tech 30
	year = 1805
	expects_institution = {
		feudalism = 0.5
		renaissance = 0.5
		new_world_i = 0.5
		printing_press = 0.5
		global_trade = 0.5
		manufactories = 0.5
		enlightenment = 0.5
		industrialization = 1
	}
	
	#Improved Draft Animals
	production_efficiency	=	0.02
}

technology = {
	# Tech 31
	year = 1820
	expects_institution = {
		feudalism = 0.5
		renaissance = 0.5
		new_world_i = 0.5
		printing_press = 0.5
		global_trade = 0.5
		manufactories = 0.5
		enlightenment = 1
		industrialization = 1
	}
	
	revolutionary_republic = yes
	revolutionary_empire = yes
	governing_capacity = 500
}

technology = {
	# Tech 32
	year = 1835
	expects_institution = {
		feudalism = 0.5
		renaissance = 0.5
		new_world_i = 0.5
		printing_press = 0.5
		global_trade = 0.5
		manufactories = 0.5
		enlightenment = 1
		industrialization = 1
	}
	
	#Four field rotation
	production_efficiency	=	0.02
}
